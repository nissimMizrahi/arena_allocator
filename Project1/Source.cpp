#include <vector>
#include <stdio.h>
#include <time.h>
#include <algorithm>

#include "SimpleAllocator.h"



int main()
{

	SimpleAllocator all(1 << 30);
	std::vector<SimpleAllocator::Ptr<int>> nums1;

	
	
	for (int i = 0; i < 100; i++)
	{
		nums1.emplace_back(all.allocate<int>(20));
	}
	
	int erased = 0;
	{
		int i = 0;
		nums1.erase(std::remove_if(nums1.begin(), nums1.end(),
		[&i, &erased](auto& ptr) {
			
			if (++i & 1)
			{
				erased++;
				return true;
			}

			return false;
	
		}), nums1.end());
	}
	

	auto start = clock();
	for (int i = 0; i < (1 << 20) / (sizeof(int) * 4); i++)
	{
		auto nums = all.allocate<int>(i + 3);
		//auto nums = new int[i + 3];
		nums[0] = i;
		nums[1] = i + 1;
		nums[2] = i + 2;
		//delete[] nums;
	}

	auto end = clock() - start;
	nums1.clear();
	printf("%d", end);


	int x = 0;
	return 0;
}




