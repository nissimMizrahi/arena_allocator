#pragma once
#include <vector>
#include <stdio.h>
#include <time.h>
#include <algorithm>
#include <iostream>
#include <stdexcept>

class SimpleAllocator
{
public:
	template<class T>
	class Ptr
	{
		friend class SimpleAllocator;

		SimpleAllocator* allocator;
		T* addr;
		size_t length;

	public:

		inline size_t get_size() const { return length * sizeof(T); };
		inline char* get_last_addr() const { return  (char*)addr + get_size(); };

		T& operator[](size_t index);
		Ptr();
		~Ptr();

		Ptr(Ptr<T>&& other);
		Ptr<T>& operator=(Ptr<T>&& other);
	};

	struct Bubble
	{
		void* addr;
		size_t size;

		inline char* get_last_addr() const { return  (char*)addr + size; };
	};

	template<class T>
	struct DeallocBlock
	{
		Bubble* start_node;
		Bubble* end_node;

		size_t start_of_search;
		size_t middle_of_search;

		const Ptr<T>* deallocation_ptr;
	};

	template<class T>
	friend class Ptr;

	std::vector<Bubble> freeBubbles;
	void* arena;
	
	/*
	adds a new bubble based on the deallocation block
	*/
	template<class T>
	void addNewBubble(const DeallocBlock<T>& deallocation_block);

	/*
	allocates {number_of_elements} * sizof(T) bytes from {src_ubble}
	*/
	template<class T>
	Ptr<T> takeFromBubble(Bubble& src_ubble, size_t number_of_elements);

	/*
	returns true ig the start of the binary search needs to be shifted
	*/
	template<class T>
	bool handleBubbleRelation(DeallocBlock<T>& deallocation_block, SimpleAllocator::Bubble& related_bubble);


	/*
	deallocates a deallocation block
	*/
	template<class T>
	void handleDealocation(DeallocBlock<T>& deallocation_block);


	/*
	changes the deallocation block based on the current iteration on the free bubbles
	*/
	template<class T>
	void handleBinarySearchIteration(DeallocBlock<T>& deallocation_block, size_t& current_iteration_len);

	/*
	deallocates a pointer
	*/
	template<class T>
	void deallocate(Ptr<T>* ptr);

public:

	SimpleAllocator(size_t size, size_t expected_free_bubbles = 100);
	SimpleAllocator(void* buffer, size_t size, size_t expected_free_bubbles = 100);
	~SimpleAllocator();

	template<class T>
	Ptr<T> allocate(size_t num);
};


template<class T>
inline void SimpleAllocator::addNewBubble(const DeallocBlock<T>& deallocation_block)
{
	size_t insertion_index = deallocation_block.start_of_search;
	freeBubbles.insert(freeBubbles.begin() + insertion_index, Bubble{
		reinterpret_cast<void*>(deallocation_block.deallocation_ptr->addr),
		deallocation_block.deallocation_ptr->get_size()
	});
}

template<class T>
inline SimpleAllocator::Ptr<T> SimpleAllocator::takeFromBubble(Bubble& src_bubble, size_t number_of_elements)
{
	Ptr<T> ret_ptr;

	ret_ptr.addr = (T*)src_bubble.addr;
	ret_ptr.allocator = this;
	ret_ptr.length = number_of_elements;

	src_bubble.addr = (char*)src_bubble.addr + ret_ptr.get_size();
	src_bubble.size -= ret_ptr.get_size();

	return ret_ptr;
}

template<class T>
bool SimpleAllocator::handleBubbleRelation(DeallocBlock<T>& deallocation_block, SimpleAllocator::Bubble& related_bubble)
{
	auto ptr = deallocation_block.deallocation_ptr;

	if (related_bubble.get_last_addr() <= (char*)ptr->addr){
		if (related_bubble.get_last_addr() == (char*)ptr->addr) {
			deallocation_block.start_node = &related_bubble;
		}

		return true;
	}

	if ((char*)related_bubble.addr == ptr->get_last_addr()){
		deallocation_block.end_node = &related_bubble;
	}

	return false;
}

template<class T>
inline void SimpleAllocator::handleDealocation(DeallocBlock<T>& deallocation_block)
{
	auto start_node = deallocation_block.start_node;
	auto end_node = deallocation_block.end_node;
	auto ptr = deallocation_block.deallocation_ptr;

	size_t ptr_size = ptr->get_size();

	if (start_node && end_node){
		start_node->size += ptr_size + end_node->size;

		auto end_index = end_node - freeBubbles.data();
		freeBubbles.erase(freeBubbles.begin() + end_index);
	}

	else if (start_node){
		start_node->size += ptr_size;
	}
	else if (end_node){
		end_node->addr = ptr->addr;
		end_node->size += ptr_size;
	}
	else{
		addNewBubble(deallocation_block);
	}
}


template<class T>
SimpleAllocator::Ptr<T> SimpleAllocator::allocate(size_t length)
{
	auto bubbleToDelete = freeBubbles.end();
	size_t allocation_size = length * sizeof(T);

	for (auto bubble_it = freeBubbles.begin(); bubble_it != freeBubbles.end(); bubble_it++){
		
		if (allocation_size <= bubble_it->size){
			Ptr<T> new_ptr = takeFromBubble<T>(*bubble_it, length);

			if (bubble_it->size == 0) {
				freeBubbles.erase(bubble_it);
			}

			return std::move(new_ptr);
		}
	}

	throw std::exception("allocator has no more memory");
}


template<class T>
inline void SimpleAllocator::handleBinarySearchIteration(DeallocBlock<T>& deallocation_block, size_t& current_iteration_len)
{
	deallocation_block.middle_of_search = current_iteration_len >> 1;

	size_t bubble_index = deallocation_block.start_of_search + deallocation_block.middle_of_search;
	auto& middle_bubble = freeBubbles[bubble_index];

	bool advance_start = handleBubbleRelation<T>(deallocation_block, middle_bubble);
	if (advance_start) {
		deallocation_block.start_of_search += deallocation_block.middle_of_search + 1;
		current_iteration_len -= deallocation_block.middle_of_search + 1;
	}
	else {
		current_iteration_len = deallocation_block.middle_of_search;
	}
}


template<class T>
void SimpleAllocator::deallocate(Ptr<T>* ptr)
{
	DeallocBlock<T> deallocation_block = { 0 };

	size_t last_search_len = freeBubbles.size();
	deallocation_block.deallocation_ptr = ptr;

	while (last_search_len)
	{
		handleBinarySearchIteration(deallocation_block, last_search_len);
	}

	handleDealocation(deallocation_block);
}

template<class T>
T& SimpleAllocator::Ptr<T>::operator[](size_t index)
{
	if (!(index >= 0 && index < length)) {
		throw std::out_of_range("access violation");
	}

	return addr[index];
}

template<class T>
SimpleAllocator::Ptr<T>::Ptr::Ptr() : addr(NULL), length(NULL), allocator(NULL)
{
}


template<class T>
SimpleAllocator::Ptr<T>::Ptr::Ptr(Ptr<T>&& other) : Ptr()
{
	*this = std::move(other);
}

template<class T>
SimpleAllocator::Ptr<T>& SimpleAllocator::Ptr<T>::operator=(Ptr<T>&& other)
{
	if (this->length && this->addr && this->allocator)
	{
		this->allocator->deallocate(this);
	}

	this->allocator = other.allocator;
	this->length = other.length;
	this->addr = other.addr;

	other.addr = NULL;
	other.length = NULL;

	return *this;
}

template<class T>
SimpleAllocator::Ptr<T>::~Ptr()
{
	if (addr != NULL && length != NULL)
		allocator->deallocate(this);
}

